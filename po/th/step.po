# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Phuwanat Sakornsakolpat <narachai@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-04 00:38+0000\n"
"PO-Revision-Date: 2011-05-12 22:36+0700\n"
"Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 1.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ภูวณัฏฐ์ สาครสกลพัฒน์"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "narachai@gmail.com"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: step/configure_controller.ui:28 step/configure_graph.ui:28
#: step/configure_meter.ui:28
#, kde-format
msgid "Data Source"
msgstr "แหล่งข้อมูล"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: step/configure_controller.ui:47
#, kde-format
msgid "Range"
msgstr "ช่วง"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: step/configure_controller.ui:59
#, kde-format
msgid "min:"
msgstr "ต่ำสุด:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: step/configure_controller.ui:75
#, kde-format
msgid "max:"
msgstr "สูงสุด:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditMinX)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditMinY)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditMin)
#: step/configure_controller.ui:97 step/configure_graph.ui:135
#: step/configure_graph.ui:187
#, kde-format
msgid "0"
msgstr "0"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditMass)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditBodyMass)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditMaxX)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditMaxY)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditMax)
#: step/configure_controller.ui:119 step/configure_graph.ui:151
#: step/configure_graph.ui:203 step/create_gas_particles.ui:125
#: step/create_softbody_items.ui:78
#, kde-format
msgid "1"
msgstr "1"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: step/configure_controller.ui:132
#, kde-format
msgid "Shortcuts"
msgstr "ปุ่มลัด"

#. i18n: ectx: property (text), widget (QLabel, label)
#: step/configure_controller.ui:138
#, kde-format
msgid "Decrease:"
msgstr "ลดลง:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: step/configure_controller.ui:148
#, kde-format
msgid "Increase:"
msgstr "เพิ่มขึ้น:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: step/configure_controller.ui:179
#, kde-format
msgid "Increment:"
msgstr "ส่วนเพิ่ม:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditIncrement)
#: step/configure_controller.ui:192
#, kde-format
msgid "0.1"
msgstr "0.1"

#. i18n: ectx: property (text), widget (QLabel, label)
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: step/configure_graph.ui:34 step/configure_graph.ui:109
#, kde-format
msgid "X :"
msgstr "X :"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#. i18n: ectx: property (text), widget (QLabel, label_4)
#: step/configure_graph.ui:51 step/configure_graph.ui:161
#, kde-format
msgid "Y :"
msgstr "Y :"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: step/configure_graph.ui:71
#, kde-format
msgid "Ranges"
msgstr "ช่วง"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: step/configure_graph.ui:83
#, kde-format
msgid "Min:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: step/configure_graph.ui:99
#, kde-format
msgid "Max:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxAutoX)
#. i18n: ectx: property (text), widget (QCheckBox, checkBoxAutoY)
#: step/configure_graph.ui:122 step/configure_graph.ui:174
#, kde-format
msgid "auto"
msgstr "อัตโนมัติ"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: step/configure_graph.ui:216 step/configure_meter.ui:47
#: step/create_gas_particles.ui:28 step/create_softbody_items.ui:28
#, kde-format
msgid "Options"
msgstr "ตัวเลือก"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowLines)
#: step/configure_graph.ui:222
#, kde-format
msgid "Show lines"
msgstr "แสดงเส้น"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowPoints)
#: step/configure_graph.ui:229
#, kde-format
msgid "Show points"
msgstr "แสดงจุด"

#. i18n: ectx: property (text), widget (QLabel, label)
#: step/configure_meter.ui:53
#, kde-format
msgid "Number of digits:"
msgstr "จำนวนหลัก:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: step/configure_step_general.ui:16
#, kde-format
msgid "Display precision:"
msgstr "ความเที่ยงที่แสดงผล:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_showAxes)
#: step/configure_step_general.ui:52
#, kde-format
msgid "Show scene axes"
msgstr "แสดงแกนของฉาก"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_showCreationTips)
#: step/configure_step_general.ui:59
#, kde-format
msgid "Show tips when creating objects"
msgstr "แสดงเคล็บลับเมื่อสร้างวัตถุ"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_enableOpenGL)
#: step/configure_step_general.ui:66
#, kde-format
msgid "Enable OpenGL (EXPERIMENTAL)"
msgstr "เปิดใช้งาน OpenGL (ทดลอง)"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_wikiExternal)
#: step/configure_step_general.ui:73
#, kde-format
msgid "Open wikipedia in external browser"
msgstr "เปิดวิกิพีเดียในเบราว์เซอร์ภายนอก"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: step/create_gas_particles.ui:34
#, fuzzy, kde-format
#| msgid "Area"
msgid "Area:"
msgstr "พื้นที่"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEditVolume)
#: step/create_gas_particles.ui:41
#, kde-format
msgid "The area the gas takes"
msgstr "พื้นที่ที่แก๊สกระจายตัว"

#. i18n: ectx: property (text), widget (QLabel, label)
#: step/create_gas_particles.ui:61
#, kde-format
msgid "Particle count:"
msgstr "จำนวนอนุภาค:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEditCount)
#. i18n: ectx: property (whatsThis), widget (QLineEdit, lineEditCount)
#: step/create_gas_particles.ui:68 step/create_gas_particles.ui:71
#, kde-format
msgid "The number of particles of this gas."
msgstr "จำนวนอนุภาคของแก๊สนี้"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditCount)
#: step/create_gas_particles.ui:74
#, kde-format
msgid "20"
msgstr "20"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: step/create_gas_particles.ui:91
#, kde-format
msgid "Concentration:"
msgstr "ความเข้มข้น:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: step/create_gas_particles.ui:112
#, kde-format
msgid "Particle mass:"
msgstr "มวลของอนุภาค:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEditMass)
#. i18n: ectx: property (whatsThis), widget (QLineEdit, lineEditMass)
#: step/create_gas_particles.ui:119 step/create_gas_particles.ui:122
#, kde-format
msgid "The mass of one particle"
msgstr "มวลของหนึ่งอนุภาค"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: step/create_gas_particles.ui:142
#, kde-format
msgid "Temperature:"
msgstr "อุณหภูมิ:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditTemperature)
#: step/create_gas_particles.ui:149
#, kde-format
msgid "1e21"
msgstr "1e21"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: step/create_gas_particles.ui:166
#, kde-format
msgid "Mean velocity:"
msgstr "ความเร็วเฉลี่ย:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditMeanVelocity)
#. i18n: ectx: property (text), widget (QLineEdit, lineEditPosition)
#: step/create_gas_particles.ui:173 step/create_softbody_items.ui:44
#, kde-format
msgid "(0,0)"
msgstr "(0,0)"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: step/create_softbody_items.ui:37
#, kde-format
msgid "Position:"
msgstr "ตำแหน่ง:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: step/create_softbody_items.ui:54
#, kde-format
msgid "Body size:"
msgstr "ขนาดของส่วนประกอบหลัก:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditSize)
#: step/create_softbody_items.ui:61
#, kde-format
msgid "(1,1)"
msgstr "(1,1)"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: step/create_softbody_items.ui:71
#, kde-format
msgid "Body mass:"
msgstr "มวลของส่วนประกอบหลัก:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: step/create_softbody_items.ui:88
#, kde-format
msgid "Young's modulus:"
msgstr "มอดุลัสของยัง:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditYoungModulus)
#: step/create_softbody_items.ui:95
#, kde-format
msgid "15"
msgstr "15"

#. i18n: ectx: property (text), widget (QLabel, label1)
#: step/create_softbody_items.ui:105
#, kde-format
msgid "Body damping:"
msgstr "ความหน่วงของส่วนประกอบหลัก:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditBodyDamping)
#: step/create_softbody_items.ui:112
#, kde-format
msgid "0.3"
msgstr "0.3"

#. i18n: ectx: property (text), widget (QLabel, label2)
#: step/create_softbody_items.ui:122
#, kde-format
msgid "Body split counts:"
msgstr "จำนวนการแบ่งของส่วนประกอบหลัก:"

#. i18n: ectx: property (text), widget (QLineEdit, lineEditSplit)
#: step/create_softbody_items.ui:129
#, kde-format
msgid "(4,4)"
msgstr "(4,4)"

#: step/gascreationdialog.cc:31
#, fuzzy, kde-format
#| msgid "Create gas particles"
msgctxt "@title:window"
msgid "Create Gas Particles"
msgstr "สร้างอนุภาคแก๊ส"

#: step/gasgraphics.cc:31
#, kde-format
msgid ""
"Press left mouse button to position\n"
"top left corner of a region for %1"
msgstr ""
"คลิกเมาส์ซ้ายเพื่อวาางตำแหน่งที่\n"
"มุมซ้ายบนของบริเวณสำหรับ %1"

#: step/gasgraphics.cc:43 step/motorgraphics.cc:30 step/motorgraphics.cc:211
#: step/polygongraphics.cc:161 step/polygongraphics.cc:295
#: step/polygongraphics.cc:440 step/softbodygraphics.cc:50
#: step/worldgraphics.cc:58 step/worldgraphics.cc:108
#, kde-format
msgid "Create %1"
msgstr "สร้าง %1"

#: step/gasgraphics.cc:59
#, kde-format
msgid ""
"Move mouse and release left mouse button to position\n"
"bottom right corner of the region for %1"
msgstr ""
"เลื่อนเมาส์และปล่อยเมาส์ซ้ายเพื่อวางตำแหน่งที่\n"
"มุมขวาล่างของบริเวณสำหรับ %1"

#: step/gasgraphics.cc:85
#, kde-format
msgid "Please fill in the parameters for the gas particles."
msgstr "กรุณาป้อนตัวแปรเสิรมสำหรับอนุภาคแก๊ส"

#: step/gasgraphics.cc:94 step/polygongraphics.cc:196
#: step/polygongraphics.cc:335 step/polygongraphics.cc:490
#: step/softbodygraphics.cc:67 step/worldgraphics.cc:75
#: step/worldgraphics.cc:122 step/worldgraphics.cc:157
#, kde-format
msgid "%1 named '%2' created"
msgstr "%1 ชื่อ '%2' ถูกสร้างแล้ว"

#: step/gasgraphics.cc:282
#, fuzzy, kde-format
#| msgid "Create particles..."
msgid "Create Particles..."
msgstr "สร้างอนุภาค..."

#: step/gasgraphics.cc:352
#, kde-format
msgid ""
"You are trying to create a very large number of particles. This will make "
"simulation very slow. Do you want to continue?"
msgstr ""
"คุณกำลังจะสร้างอนุภาคจำนวนมหาศาล ซึ่งจะทำให้การจำลองช้ามาก ต้องการดำเนินการต่อไปหรือไม่?"

#: step/gasgraphics.cc:354 step/mainwindow.cc:407
#, kde-format
msgid "Warning - Step"
msgstr "คำเตือน - Step"

#: step/gasgraphics.cc:365
#, kde-format
msgid "Create particles for %1"
msgstr "สร้างอนุภาคสำหรับ %1"

#: step/infobrowser.cc:26
#, fuzzy, kde-format
#| msgid "Context info"
msgid "Context Info"
msgstr "ข้อมูลของบริบท"

#: step/infobrowser.cc:44
#, kde-format
msgid "Back"
msgstr "กลับ"

#: step/infobrowser.cc:46
#, kde-format
msgid "Forward"
msgstr "ถัดไป"

#: step/infobrowser.cc:50
#, kde-format
msgid "Sync selection"
msgstr "ปรับการเลือกให้ตรงกัน"

#: step/infobrowser.cc:52
#, kde-format
msgid "Follow selection"
msgstr "ตามการเลือก"

#: step/infobrowser.cc:57
#, fuzzy, kde-format
#| msgid "Open in browser"
msgid "Open in Browser"
msgstr "เปิดในเบราว์เซอร์"

#: step/infobrowser.cc:132
#, kde-format
msgid "Documentation"
msgstr "คู่มือการใช้"

#: step/infobrowser.cc:137
#, kde-format
msgid "No current object."
msgstr "ไม่มีวัตถุปัจจุบัน"

#: step/infobrowser.cc:159
#, kde-format
msgid "Documentation error"
msgstr "คู่มือการใช้ผิดพลาด"

#: step/infobrowser.cc:164
#, kde-format
msgid "Documentation for %1 not available. "
msgstr "ไม่มีคู่มือของ %1"

#: step/infobrowser.cc:165
#, fuzzy, kde-format
#| msgid ""
#| "You can help <a href=\"http://edu.kde.org/step\">Step</a> by writing it!"
msgid ""
"You can help <a href=\"https://edu.kde.org/step\">Step</a> by writing it!"
msgstr "คุณสามารช่วย <a href=\"http://edu.kde.org/step\">Step</a> ได้โดยเขียนมัน"

#: step/itempalette.cc:177
#, kde-format
msgid "Palette"
msgstr "จานสี"

#: step/itempalette.cc:196
#, kde-format
msgid "Pointer"
msgstr "ตัวชี้"

#: step/itempalette.cc:197
#, kde-format
msgid "Selection pointer"
msgstr "ตัวชี้การเลือก"

#: step/itempalette.cc:220
#, kde-format
msgid "Show text"
msgstr "แสดงข้อความ"

#: step/jointgraphics.cc:158 step/springgraphics.cc:30
#, kde-format
msgid "Move end of %1"
msgstr "ย้ายจุดจบของ %1"

#: step/latexformula.cc:30
#, kde-format
msgid "can not launch %1"
msgstr "ไม่สามารถเริ่ม %1"

#: step/latexformula.cc:40
#, kde-format
msgid "error running %1"
msgstr "มีข้อผิดพลาดนการเปิด %1"

#: step/latexformula.cc:44
#, kde-format
msgid ""
"%1 reported an error (exit status %2):\n"
"%3"
msgstr ""
"%1 ได้รายงานข้อผิดพลาด (สถานะออก %2):\n"
"%3"

#: step/latexformula.cc:49
#, kde-format
msgid "%1 did not create output file"
msgstr "%1 ไม่ได้สร้างแฟ้มผลลัพธ์"

#: step/latexformula.cc:81
#, kde-format
msgid "can not open temporary file"
msgstr "ไม่สามารถเปิดแฟ้มชั่วคราาได้"

#: step/latexformula.cc:115
#, kde-format
msgid "can not open result file"
msgstr "ไม่สามารถเปิดแฟ้มผลได้"

#: step/main.cc:31
#, kde-format
msgid "Step"
msgstr "Step"

#: step/main.cc:33
#, kde-format
msgid "Interactive physical simulator"
msgstr "ตัวจำลองการทดลองทางฟิสิกส์แบบโต้ตอบได้"

#: step/main.cc:35
#, kde-format
msgid "(C) 2007 Vladimir Kuznetsov"
msgstr "สงวนลิทธิ์ 2550 โดย Vladimir Kuznetsov"

#: step/main.cc:37
#, kde-format
msgid "https://edu.kde.org/step"
msgstr ""

#: step/main.cc:42
#, kde-format
msgid "Original author"
msgstr "ผู้เขียน"

#: step/main.cc:43
#, kde-format
msgid "ks.vladimir@gmail.com"
msgstr ""

#: step/main.cc:48
#, kde-format
msgid "Code contributions"
msgstr "ส่วนร่วมในโค้ด"

#: step/main.cc:49
#, kde-format
msgid "cniehaus@kde.org"
msgstr ""

#: step/main.cc:59
#, kde-format
msgid "Document to open"
msgstr "เอกสารที่จะเปิด"

#: step/mainwindow.cc:143
#, kde-format
msgid "&Open Tutorial..."
msgstr "เปิดตำ&รา..."

#: step/mainwindow.cc:148
#, kde-format
msgid "&Open Example..."
msgstr "เปิดตัว&อย่าง..."

#: step/mainwindow.cc:153
#, kde-format
msgid "Open Down&loaded Example..."
msgstr "เปิดตัว&อย่างที่ดาวน์โหลดมาแล้ว..."

#: step/mainwindow.cc:158
#, kde-format
msgid "Share C&urrent Experiment..."
msgstr "แบ่งปันการ&ทดลองปัจจุบัน..."

#: step/mainwindow.cc:161
#, kde-format
msgid "&Download New Experiments..."
msgstr "ดาวน์โหลดการทดลองใ&หม่"

#: step/mainwindow.cc:168
#, kde-format
msgid "Redo"
msgstr "ทำซ้ำ"

#: step/mainwindow.cc:168
#, kde-format
msgid "Undo"
msgstr "เรียกคืน"

#: step/mainwindow.cc:190
#, kde-format
msgid "&Delete"
msgstr "&ลบ"

#: step/mainwindow.cc:200
#, kde-format
msgid "&Run"
msgstr "ดำเนิน&งาน"

#: step/mainwindow.cc:206
#, kde-format
msgid "Execute the program"
msgstr "สั่งให้โปรแกรมกระทำการ"

#: step/mainwindow.cc:207
#, kde-format
msgid "Run: Execute the program"
msgstr "ดำเนินงาน: สั่งให้โปรแกรมกระทำการ"

#: step/mainwindow.cc:209
#, kde-format
msgctxt "@option:radio"
msgid "1x Speed"
msgstr "เร็ว 1X"

#: step/mainwindow.cc:217
#, kde-format
msgctxt "@option:radio choose the slow speed"
msgid "2x Speed"
msgstr "เร็ว 2X"

#: step/mainwindow.cc:224
#, kde-format
msgctxt "@option:radio"
msgid "4x Speed"
msgstr "เร็ว 4X"

#: step/mainwindow.cc:231
#, kde-format
msgctxt "@option:radio"
msgid "8x Speed"
msgstr "เร็ว 8x"

#: step/mainwindow.cc:238
#, kde-format
msgctxt "@option:radio"
msgid "16x Speed"
msgstr "เร็ว 16x"

#: step/mainwindow.cc:268
#, kde-format
msgctxt "filename"
msgid "untitled.step"
msgstr "ไม่มีชื่อ.step"

#: step/mainwindow.cc:294
#, kde-format
msgid "<new file>"
msgstr "<แฟ้มใหม่>"

#: step/mainwindow.cc:308
#, kde-format
msgctxt "@title:window"
msgid "Open Step File"
msgstr ""

#: step/mainwindow.cc:308 step/mainwindow.cc:343
#, fuzzy, kde-format
#| msgid "*.step|Step files (*.step)"
msgid "Step files (*.step)"
msgstr "*.step|แฟ้มของ Step (*.step)"

#: step/mainwindow.cc:318 step/mainwindow.cc:359
#, kde-format
msgid "Cannot open file '%1'"
msgstr "ไม่สามารถเปิดแฟ้ม '%1' ได้"

#: step/mainwindow.cc:323
#, kde-format
msgid "Cannot parse file '%1': %2"
msgstr "ไม่สามารถแจงแฟ้ม '%1': %2"

#: step/mainwindow.cc:332
#, kde-format
msgid "<open file: %1>"
msgstr "<เปิดแฟ้ม: %1>"

#: step/mainwindow.cc:343
#, kde-format
msgctxt "@title:window"
msgid "Save Step File"
msgstr ""

#: step/mainwindow.cc:365
#, kde-format
msgid "Cannot save file '%1': %2"
msgstr "ไม่สามารถบันทกแฟ้ม '%1': %2"

#: step/mainwindow.cc:406
#, kde-format
msgid ""
"The experiment has been modified.\n"
"Do you want to save your changes?"
msgstr ""
"มีการแก้ไขการทดลอง\n"
"คุณต้องการที่จะบันทึกการเปลี่ยนแปลงหรือไม่?"

#: step/mainwindow.cc:458
#, kde-format
msgid "Uploading is still not implemented in kdelibs."
msgstr "การอัพโหลดยังไม่ถูกรวมใน kdelibs"

#: step/mainwindow.cc:459
#, kde-format
msgid "Sorry - Step"
msgstr "ข้ออภัย - Step"

#: step/mainwindow.cc:486
#, kde-format
msgid "&Stop"
msgstr "หยุ&ด"

#: step/mainwindow.cc:496
#, kde-format
msgid "&Simulate"
msgstr "จำ&ลอง"

#: step/mainwindow.cc:501
#, kde-format
msgid ""
"Cannot finish this step because local error is greater than local "
"tolerance.\n"
"Please check solver settings and try again."
msgstr ""
"ทำขั้นตอนนี้ไม่สำเร็จเนื่องจากความผิดพลาดภายในมากกว่าคามทนภายใน\n"
"กรุณาตรวจสอบการตั้งค่าตัวแก้และลองอีกครั้ง"

#: step/mainwindow.cc:506
#, kde-format
msgid ""
"Cannot finish this step because there are collisions which cannot be "
"resolved automatically.\n"
"Please move colliding objects apart and try again."
msgstr ""
"ทำขั้นตอนนี้ไม่สำเร็จเนื่องจากเกิดการชนที่แก้ไขด้วยระบบอัตโนมัติไม่ได้\n"
"กรุณาแยกวัตถุที่ชนกันและลองอีกครั้ง"

#: step/mainwindow.cc:510
#, kde-format
msgid "Cannot finish this step because of an unknown error."
msgstr "ทำขั้นตอนนี้ไม่สำเร็จเนื่องจากพบข้อผิดพลาดที่ไม่ทราบสาเหตุ"

#: step/mainwindow.cc:543
#, kde-format
msgid "&Undo"
msgstr "เรีย&กคืน"

#: step/mainwindow.cc:544
#, kde-format
msgid "&Undo: %1"
msgstr "เรีย&กคืน: %1"

#: step/mainwindow.cc:549
#, kde-format
msgid "Re&do"
msgstr "ทำ&ซ้ำ"

#: step/mainwindow.cc:550
#, kde-format
msgid "Re&do: %1"
msgstr "ทำ&ซ้ำ: %1"

#: step/mainwindow.cc:581
#, kde-format
msgid "General"
msgstr "ทั่วไป"

#: step/motorgraphics.cc:100 step/motorgraphics.cc:278
#: step/stepgraphicsitem.cc:198 step/stepgraphicsitem.cc:210
#, kde-format
msgid "Move %1"
msgstr "ย้าย %1"

#: step/polygongraphics.cc:149
#, kde-format
msgid "Press left mouse button to position a center of a %1"
msgstr "กดเมาส์ซ้ายเพื่อวางตำแหน่งของจุดศูนย์กลางของ %1"

#: step/polygongraphics.cc:170
#, kde-format
msgid "Move mouse and release left mouse button to define a radius of the %1"
msgstr "เลื่อนเมาส์และปล่อยเมาส์ซ้ายเพื่อกำหนดรัศมีของ %1"

#: step/polygongraphics.cc:283
#, kde-format
msgid ""
"Press left mouse button to position\n"
"top left corner of a %1"
msgstr ""
"คลิกเมาส์ซ้ายเพื่อวางตำแหน่งที่\n"
"มุมซ้ายบนของ %1"

#: step/polygongraphics.cc:305
#, kde-format
msgid ""
"Move mouse and release left mouse button to position\n"
"bottom right corner of the %1"
msgstr ""
"เลื่อนเมาส์และปล่อยเมาส์ซ้ายเพื่อวางตำแหน่งที่\n"
"มุมขวาล่างของ %1"

#: step/polygongraphics.cc:428
#, kde-format
msgid "Click on the scene to create a first vertex of %1"
msgstr "คลิกบนฉากเพื่อสร้างจุดยอดแรกของ %1"

#: step/polygongraphics.cc:476
#, kde-format
msgid "Click on the scene to add new vertex or press Enter to finish"
msgstr "คลิกบนฉากเพื่อเพิ่มจุดยอดใหม่หรือกดปุ่ม Enter เพื่อจบ"

#: step/propertiesbrowser.cc:267
#, kde-format
msgid "Change solver type"
msgstr "เปลี่ยนชนิดตัวแก้ปัญหา"

#: step/propertiesbrowser.cc:282 step/propertiesbrowser.cc:288
#: step/propertiesbrowser.cc:295 step/propertiesbrowser.cc:304
#: step/propertiesbrowser.cc:388 step/propertiesbrowser.cc:405
#: step/stepgraphicsitem.cc:393 step/stepgraphicsitem.cc:538
#, kde-format
msgid "Change %1.%2"
msgstr "เปลี่ยน %1.%2"

#: step/propertiesbrowser.cc:302
#, kde-format
msgid "Rename %1 to %2"
msgstr "เปลี่ยนชื่อจาก %1 เป็น %2"

#: step/propertiesbrowser.cc:452
#, kde-format
msgid "Property"
msgstr "คุณสมบัติ"

#: step/propertiesbrowser.cc:453
#, kde-format
msgid "Value"
msgstr "ค่า"

#: step/propertiesbrowser.cc:518
#, kde-format
msgid "false"
msgstr "ไม่ใช่"

#: step/propertiesbrowser.cc:519
#, kde-format
msgid "true"
msgstr "ใช่"

#: step/propertiesbrowser.cc:683
#, kde-format
msgid "Properties"
msgstr "คุณสมบัติ"

#: step/softbodygraphics.cc:37 step/worldgraphics.cc:50
#: step/worldgraphics.cc:94
#, kde-format
msgid "Click on the scene to create a %1"
msgstr "คลิกบนฉากเพื่อสร้าง %1"

#: step/softbodygraphics.cc:54
#, kde-format
msgid "Please fill in the parameters for %1"
msgstr "กรุณาป้อนตัวแปรเสริมสำหรับ %1"

#: step/softbodygraphics.cc:114
#, fuzzy, kde-format
#| msgid "Create soft body items"
msgctxt "@title:window"
msgid "Create Soft Body Items"
msgstr "สร้างรายการวัตถุอ่อน"

#: step/softbodygraphics.cc:175
#, kde-format
msgid "Create items for %1"
msgstr "สร้างรายการสำหรับ %1"

#. i18n: ectx: label, entry (showCreationTips), group (General)
#. i18n: ectx: whatsthis, entry (showCreationTips), group (General)
#: step/step.kcfg:9 step/step.kcfg:10
#, kde-format
msgid "Show tips when creating items"
msgstr "แสดงเคล็ดลับเมื่อสร้างรายการ"

#. i18n: ectx: label, entry (showAxes), group (General)
#. i18n: ectx: whatsthis, entry (showAxes), group (General)
#: step/step.kcfg:14 step/step.kcfg:15
#, kde-format
msgid "Show axes on the scene"
msgstr "แสดงแกนบนฉาก"

#. i18n: ectx: label, entry (floatDisplayPrecision), group (General)
#. i18n: ectx: whatsthis, entry (floatDisplayPrecision), group (General)
#: step/step.kcfg:19 step/step.kcfg:20
#, kde-format
msgid "Display precision of double numbers"
msgstr "แสดงคาามเที่ยงของเลขทศนิยม"

#. i18n: ectx: label, entry (enableOpenGL), group (General)
#. i18n: ectx: whatsthis, entry (enableOpenGL), group (General)
#: step/step.kcfg:26 step/step.kcfg:27
#, kde-format
msgid "Use OpenGL to accelerate drawing whenever possible"
msgstr "หากเป็นไปได้ให้ใช้ OpenGL เร่งการวาด"

#. i18n: ectx: label, entry (wikiExternal), group (InfoBrowser)
#. i18n: ectx: whatsthis, entry (wikiExternal), group (InfoBrowser)
#: step/step.kcfg:33 step/step.kcfg:34
#, kde-format
msgid "Browse wikipedia in external browser"
msgstr "เปิดวิกิพีเดียในเบราว์เซอร์ภายนอก"

#. i18n: ectx: label, entry (showButtonText), group (ItemPalette)
#. i18n: ectx: whatsthis, entry (showButtonText), group (ItemPalette)
#: step/step.kcfg:40 step/step.kcfg:41
#, kde-format
msgid "Show text beside icon"
msgstr "แสดงข้อความข้างไอคอน"

#: step/stepgraphicsitem.cc:210
#, kde-format
msgid "several objects"
msgstr "วัตถุหลายชิ้น"

#: step/stepgraphicsitem.cc:394 step/stepgraphicsitem.cc:540
#, kde-format
msgid "Change %1"
msgstr "เปลี่ยน %1"

#. i18n: ectx: ToolBar (mainToolBar)
#: step/stepui.rc:5
#, kde-format
msgid "Main Toolbar"
msgstr "แถบเครื่องมือหลัก"

#. i18n: ectx: ToolBar (simulationToolBar)
#: step/stepui.rc:10
#, kde-format
msgid "Simulation Toolbar"
msgstr "แถบเครื่องมือการจำลอง"

#. i18n: ectx: Menu (examples)
#: step/stepui.rc:19
#, kde-format
msgid "&Examples"
msgstr "ตัว&อย่าง"

#. i18n: ectx: Menu (simulation)
#: step/stepui.rc:30
#, kde-format
msgid "&Simulation"
msgstr "การจำ&ลอง"

#. i18n: ectx: Menu (speed)
#: step/stepui.rc:33
#, kde-format
msgid "&Run Speed"
msgstr "&ความเร็ว"

#. i18n: ectx: Menu (panels)
#: step/stepui.rc:43
#, kde-format
msgid "&Panels"
msgstr "แถบพาแ&นล"

#: step/toolgraphics.cc:256
#, kde-format
msgid "Click to enter text"
msgstr "คลิกเพื่อป้อนข้อความ"

#: step/toolgraphics.cc:381
#, kde-format
msgid "&Color"
msgstr "&สี"

#: step/toolgraphics.cc:383
#, kde-format
msgid "&Bold"
msgstr "ตัว&หนา"

#: step/toolgraphics.cc:385
#, kde-format
msgid "&Italic"
msgstr "ตัวเอีย&ง"

#: step/toolgraphics.cc:387
#, kde-format
msgid "&Underline"
msgstr "ขี&ดเส้นใต้"

#: step/toolgraphics.cc:390
#, kde-format
msgid "Align &Left"
msgstr "ชิดซ้า&ย"

#: step/toolgraphics.cc:392
#, kde-format
msgid "Align C&enter"
msgstr "ตรง&กลาง"

#: step/toolgraphics.cc:394
#, kde-format
msgid "Align &Right"
msgstr "ชิด&ขวา"

#: step/toolgraphics.cc:396
#, kde-format
msgid "Align &Justify"
msgstr "กระ&จาย"

#: step/toolgraphics.cc:399
#, kde-format
msgid "&Align"
msgstr "จัดตำแ&หน่ง"

#: step/toolgraphics.cc:408
#, kde-format
msgid "&Font"
msgstr "แ&บบอักษร"

#: step/toolgraphics.cc:409
#, kde-format
msgid "Font &Size"
msgstr "&ขนาดอักษร"

#: step/toolgraphics.cc:411
#, fuzzy, kde-format
#| msgid "Insert &Image"
msgid "Insert &Image..."
msgstr "แทรกรูป&ภาพ"

#: step/toolgraphics.cc:416
#, fuzzy, kde-format
#| msgid "Insert &Formula"
msgid "Insert &Formula..."
msgstr "แทรกสู&ตร"

#: step/toolgraphics.cc:527
#, kde-format
msgid "Edit %1"
msgstr "แก้ไข %1"

#: step/toolgraphics.cc:632
#, kde-format
msgctxt "@title:window"
msgid "Open Image File"
msgstr ""

#: step/toolgraphics.cc:632
#, kde-format
msgid "Images (*.png *.jpg *.jpeg)"
msgstr ""

#: step/toolgraphics.cc:650
#, kde-format
msgid "Cannot parse file '%1'"
msgstr "ไม่สามารถแจงแฟ้ม '%1'"

#: step/toolgraphics.cc:694
#, kde-format
msgid ""
"Cannot find latex installation. You need 'latex', 'dvips' and 'gs' "
"executables installed and accessible from $PATH"
msgstr ""
"ไม่พบการติดตั้ง latex ซึ่งจำเป็นต้องติดตั้งและสามารถใช้งานได้ที่ $PATH พร้อมกับ dvips และ gs"

#: step/toolgraphics.cc:700
#, fuzzy, kde-format
#| msgid "LaTex Formula - Step"
msgctxt "@title:window"
msgid "LaTex Formula - Step"
msgstr "สูตร LaTex - Step"

#: step/toolgraphics.cc:701
#, fuzzy, kde-format
#| msgid "Enter LaTeX formula string"
msgid "Enter LaTeX formula string:"
msgstr "ป้อนสตริงสูตรของ LaTeX"

#: step/toolgraphics.cc:710
#, kde-format
msgid "Cannot compile LaTeX formula: %1"
msgstr "ไม่สามารถคอมไล์สูตร LaTeX: %1"

#: step/toolgraphics.cc:716
#, kde-format
msgid "Cannot parse result image"
msgstr "ไม่สามารถแจงภาพของผลลัพธ์ได้"

#: step/toolgraphics.cc:777
#, kde-format
msgid "Object name"
msgstr "ชื่อวัตถุ"

#: step/toolgraphics.cc:782
#, kde-format
msgid "Property name"
msgstr "ชื่อคุณสมบัติ"

#: step/toolgraphics.cc:788
#, kde-format
msgid "Vector index"
msgstr "ดัชนีเวกเตอร์"

#: step/toolgraphics.cc:977 step/toolgraphics.cc:985 step/toolgraphics.cc:1478
#, kde-format
msgid "%1.%2"
msgstr "%1.%2"

#: step/toolgraphics.cc:978 step/toolgraphics.cc:986 step/toolgraphics.cc:1479
#, kde-format
msgid "[%1]"
msgstr "[%1]"

#: step/toolgraphics.cc:982 step/toolgraphics.cc:990 step/toolgraphics.cc:1483
#, kde-format
msgid "[not configured]"
msgstr "[ยังไม่ได้ตั้งค่า]"

#: step/toolgraphics.cc:1078
#, fuzzy, kde-format
#| msgid "Clear graph"
msgid "Clear Graph"
msgstr "ล้างกราฟ"

#: step/toolgraphics.cc:1079
#, fuzzy, kde-format
#| msgid "Configure graph..."
msgid "Configure Graph..."
msgstr "ปรับแต่งกราฟ..."

#: step/toolgraphics.cc:1097
#, fuzzy, kde-format
#| msgid "Configure graph"
msgctxt "@title:window"
msgid "Configure Graph"
msgstr "ปรับแต่งกราฟ"

#: step/toolgraphics.cc:1173 step/toolgraphics.cc:1372
#: step/toolgraphics.cc:1710
#, kde-format
msgid "Edit properties of %1"
msgstr "แก้ไขคุณสมบัติของ %1"

#: step/toolgraphics.cc:1225
#, kde-format
msgid "Clear graph %1"
msgstr "ล้างกราฟ %1"

#: step/toolgraphics.cc:1306
#, fuzzy, kde-format
#| msgid "Configure meter..."
msgid "Configure Meter..."
msgstr "ปรับแต่งตัววัด..."

#: step/toolgraphics.cc:1324
#, fuzzy, kde-format
#| msgid "Configure meter"
msgctxt "@title:window"
msgid "Configure Meter"
msgstr "ปรับแต่งตัววัด"

#: step/toolgraphics.cc:1421 step/toolgraphics.cc:1618
#, fuzzy, kde-format
#| msgid "Increase value"
msgid "Increase Value"
msgstr "เพิ่มค่า"

#: step/toolgraphics.cc:1422 step/toolgraphics.cc:1619
#, fuzzy, kde-format
#| msgid "Decrease value"
msgid "Decrease Value"
msgstr "ลดค่า"

#: step/toolgraphics.cc:1452 step/toolgraphics.cc:1747
#, kde-format
msgid "Decrease controller %1"
msgstr "ลดตัวควบคุม %1"

#: step/toolgraphics.cc:1461 step/toolgraphics.cc:1756
#, kde-format
msgid "Increase controller %1"
msgstr "เพิ่มตัวควบคุม %1"

#: step/toolgraphics.cc:1595
#, kde-format
msgid "Change controller %1"
msgstr "เปลี่ยนตัวควบคุม %1"

#: step/toolgraphics.cc:1621
#, fuzzy, kde-format
#| msgid "Configure controller..."
msgid "Configure Controller..."
msgstr "ปรับแต่งตัวควบคุม..."

#: step/toolgraphics.cc:1639
#, fuzzy, kde-format
#| msgid "Configure controller"
msgctxt "@title:window"
msgid "Configure Controller"
msgstr "ปรับแต่งตัวควบคุม"

#: step/toolgraphics.cc:1900
#, fuzzy, kde-format
#| msgid "Clear trace"
msgid "Clear Trace"
msgstr "ล้างร่องรอย"

#: step/toolgraphics.cc:1909
#, kde-format
msgid "Clear tracer %1"
msgstr "ล้างตัวติดตาม %1"

#: step/undobrowser.cc:18
#, fuzzy, kde-format
#| msgid "Undo history"
msgid "Undo History"
msgstr "ประวัติการเรียกคืน"

#: step/worldbrowser.cc:50
#, kde-format
msgctxt "Object list"
msgid "World"
msgstr "โลก"

#: step/worldgraphics.cc:90
#, kde-format
msgid ""
"Press left mouse button to position first end of a %1\n"
"then drag and release it to position the second end"
msgstr ""
"กดเมาส์ซ้ายเพื่อวางตำแหน่งจบแรกของ %1\n"
"หลังจากนั้นให้ลากแล้วปล่อยยังตำแหน่งจบที่สอง"

#: step/worldgraphics.cc:118
#, kde-format
msgid "Release left mouse button to position second end of the %1"
msgstr "ปล่อยเมาส์ซ้ายเพื่อวางตำแหน่งจบที่สองของ %1"

#: step/worldmodel.cc:629
#, kde-format
msgid "Delete %1"
msgstr "ลบ %1"

#: step/worldmodel.cc:630
#, kde-format
msgid "Delete several items"
msgstr "ลบหลายรายการ"

#: step/worldmodel.cc:701 step/worldmodel.cc:708
#, kde-format
msgid "<no object>"
msgstr "<ไม่มีวัตถุ>"

#: step/worldmodel.cc:703
#, kde-format
msgid "<unnamed>"
msgstr "<ไม่มีชื่อ>"

#: step/worldmodel.cc:709
#, kde-format
msgid "%1: %2"
msgstr "%1: %2"

#: step/worldmodel.cc:781
#, kde-format
msgid "<nobr><h4><u>%1</u></h4></nobr>"
msgstr "<nobr><h4><u>%1</u></h4></nobr>"

#: step/worldmodel.cc:809
#, kde-format
msgid "<tr><td>%1&nbsp;&nbsp;</td><td>%2</td></tr>"
msgstr "<tr><td>%1&nbsp;&nbsp;</td><td>%2</td></tr>"

#: step/worldmodel.cc:1007
#, kde-format
msgid "Simulate %1 → %2"
msgstr "จำลอง %1 → %2"

#: step/worldmodel.cc:1060
#, kde-format
msgid "Cut %1"
msgstr "ตัด %1"

#: step/worldmodel.cc:1061
#, kde-format
msgid "Cut several items"
msgstr "ตัดหลายรายการ"

#: step/worldmodel.cc:1083
#, kde-format
msgid "Pasted %1"
msgstr "วาง %1"

#: step/worldmodel.cc:1084
#, kde-format
msgid "Pasted several items"
msgstr "วางหลายรายการ"

#: step/worldscene.cc:248
#, kde-format
msgid "Objects under mouse:"
msgstr "วัตถุที่อยู่ใต้เมาส์:"

#: step/worldscene.cc:253
#, kde-format
msgid "... (1 more item)"
msgid_plural "... (%1 more items)"
msgstr[0] "... (มีอีก %1 รายการ)"

#~ msgid "Wikipedia"
#~ msgstr "วิกิพีเดีย สารานุกรมเสรี"

#~ msgid "Fetching Wikipedia Information..."
#~ msgstr "กำลังรับข้อมูลจากวิกิพีเดีย..."

#~ msgid "Wikipedia error"
#~ msgstr "วิกิพีเดียมีข้อผิดพลาด"

#~ msgid ""
#~ "Information could not be retrieved because the server was not reachable."
#~ msgstr "ไม่ได้รับข้อมูลเนื่องจากไม่สามารถติดต่อเซิร์ฟเวอร์ได้"

#~ msgid "Wikipedia Information"
#~ msgstr "ข้อมูลจากวิกิพีเดีย"

#~ msgid "Wikipedia Other Languages"
#~ msgstr "วิกิพีเดียในภาษาอื่น"

#~ msgid "The file \"%1\" already exists. Do you wish to overwrite it?"
#~ msgstr "มีแฟ้ม \"%1\" อยู่แล้ว ต้องการจะเขียนทับมันหรือไม่?"

#~ msgid "Vladimir Kuznetsov"
#~ msgstr "Vladimir Kuznetsov"

#~ msgid "Carsten Niehaus"
#~ msgstr "Carsten Niehaus"

#~ msgid "Create items..."
#~ msgstr "สร้างรายการ..."
